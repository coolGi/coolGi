# Hey, I'm coolGi
## I am going to update this, and fix everything up later<br> This is just a placeholder

Welcome to my profile\
Im just a small dev, most known for work on [Distant Horizons](https://gitlab.com/jeseibel/distant-horizons)
<br><br><br>


Here is a spinning gif of a cat until I update my Readme properly\
![Spinning Cat Gif](https://gitlab.com/coolGi/coolGi/-/raw/main/cat-spinning.gif)

<details open>
  <summary><i>Contacts</i></summary>
  
  - <i>Website</i>: [coolgi.dev](https://coolgi.dev/)
  - <i>GitLab</i>: [coolGi](https://gitlab.com/coolGi)
  - <i>Codeberg</i>: [coolGi](https://codeberg.org/coolGi)
  - <i>GitHub</i>: [coolGi69](https://github.com/coolGi69)
  - <i>Matrix</i>: [@me:coolgi.dev](https://matrix.to/#/@me:coolgi.dev)
  - <i>Discord</i>: [@coolgi_](https://discordapp.com/users/698484271291170826)
  - <i>Youtube</i>: [coolGi](https://www.youtube.com/@coolgi133)
</details>
